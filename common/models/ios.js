var async = require("async");
module.exports = function (Ios) {
    // latest app End point /apps/latest
    Ios.latest = function (cb) {


        Ios.find(function (err, _resp) {

            if (!err) {
                var res = Math.max.apply(Math, _resp.map(function (o) {
                    return o.createdDatetimetamp;
                }))
                var obj = _resp.find(function (o) {
                    return o.createdDatetimetamp == res;
                })
                if (obj === undefined) {
                    _resp.find(function (o) {
                        return o.createdDatetimetamp == Math.max.apply(Math, _resp.map(function (o) {
                            return o.createdDatetimetamp;
                        }));
                    })
                } else
                    cb(null, obj)
            } else
                cb(err, [])

        })

    }

    Ios.remoteMethod(
        'latest', {
            http: {
                path: '/latest',
                verb: 'get'
            },
            returns: {
                arg: 'data',
                type: 'object'
            }
        }
    );
    // end lates app End Point
};
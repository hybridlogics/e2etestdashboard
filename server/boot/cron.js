var CronJob = require('cron').CronJob;
var request = require('request');
var jsonfile = require('jsonfile')

var btoa = require('btoa')
var errorFile = 'server/error.json'

var async = require("async");
var appsFile = 'server/apps.json'



module.exports = function (app) {
    var apps = app.models.apps

    //***************JSON OBJECT DEFINED BELOW 
    var errors = [
        {
            "err_name": 404,
            "status": "Resource does not exist"
  },
        {
            "err_name": 503,
            "status": "The service is currently unavailable"
  },
        {
            "err_name": 500,
            "status": "An internal error"
  },
        {
            "err_name": 401,
            "status": "Anonymous clients are not authorized to view the content"
  }
]
    var appcalls = [
          {
            "model": {
                "model": app.models.ios
            },
            "testResults": {
                "name": "ios",
                "desc": "application 1",
                "type": "testResults",
                "url": "http://10.65.113.6:8080/job/E2E_iOS/lastSuccessfulBuild/testReport/api/json?pretty=true"
            },
            "buildInfo": {
                "name": "ios",
                "desc": "application 1",
                "type": "buildInfo",
                "url": "http://10.65.113.6:8080/job/E2E_iOS/lastSuccessfulBuild/api/json?pretty=true"
            },
            "versions": "http://10.72.61.194:8080/component_all"
             },
           {
            "model": {
                "model": app.models.android
            },
            "testResults": {
                "name": "android",
                "desc": "application 2",
                "type": "testResults",
                "url": "http://10.65.113.6:8080/job/E2E_Android/lastSuccessfulBuild/testReport/api/json?pretty=true"
            },
            "buildInfo": {
                "name": "android",
                "desc": "application 2",
                "type": "buildInfo",
                "url": "http://10.65.113.6:8080/job/E2E_Android/lastSuccessfulBuild/api/json?pretty=true"
            },
            "versions": "http://10.72.61.194:8080/component_all"
            },
                {
            "model": {
                "model": app.models.web
                },
            "credentials": {
                "username": "e2edashboard",
                "password": "Ej4tcR7qk3rrRSu6"
            },
            "testResults": {
                "name": "web",
                "desc": "application 3",
                "type": "testResults",
                "url": "https://jenkins.otticlients.int.ovp.bskyb.com/job/web-e2e-tests/lastSuccessfulBuild/testReport/api/json?pretty=true"
            },
            "buildInfo": {
                "name": "web",
                "desc": "application 3",
                "type": "buildInfo",
                "url": "https://jenkins.otticlients.int.ovp.bskyb.com/job/web-e2e-tests/lastSuccessfulBuild/api/json?pretty=true"
            },
            "versions": "http://10.72.61.194:8080/component_all"
            },
              {
             "model": {
                 "model": app.models.pcms
             },
             "testResults": {
                 "name": "pcms",
                 "desc": "application 4",
                 "type": "testResults",
                 "url": "https://s1jen01-v01.pcms.int.ovp.bskyb.com/job/e2e/job/pcms-e2e-functional-tests/lastSuccessfulBuild/testReport/api/json?pretty=true"
             },
             "buildInfo": {
                 "name": "pcms",
                 "desc": "application 4",
                 "type": "buildInfo",
                 "url": "https://s1jen01-v01.pcms.int.ovp.bskyb.com/job/e2e/job/pcms-e2e-functional-tests/lastSuccessfulBuild/api/json?pretty=true"
             },
             "versions": "http://10.72.61.194:8080/component_all"
             },
             {
            "model": {
                "model": app.models.crm
            },
            "testResults": {
                "name": "crm",
                "desc": "application 5",
                "type": "testResults",
                "url": "https://crmbuild.nowtv.bskyb.com/job/quality/job/salesforce-end-to-end-tests-international/lastCompletedBuild/testReport/api/json?pretty=true"
            },
            "buildInfo": {
                "name": "crm",
                "desc": "application 5",
                "type": "buildInfo",
                "url": "https://crmbuild.nowtv.bskyb.com/job/quality/job/salesforce-end-to-end-tests-international/lastCompletedBuild/api/json?pretty=true"
            },
            "versions": "http://10.72.61.194:8080/component_all"
            },
            {
            "model": {
                "model": app.models.roku
            },
            "credentials": {
                "username": "e2eTester",
                "password": "e2eTester"
            },
            "testResults": {
                "name": "roku",
                "desc": "application 6",
                "type": "testResults",
                "url": "http://10.65.93.23:8080/job/atlas-roku-end2end-test/lastSuccessfulBuild/testReport/api/json?pretty=true"
            },
            "buildInfo": {
                "name": "roku",
                "desc": "application 6",
                "type": "buildInfo",
                "url": "http://10.65.93.23:8080/job/atlas-roku-end2end-test/lastSuccessfulBuild/api/json?pretty=true"
            },
            "versions": "http://10.72.61.194:8080/component_all"
            }
        ]

    appcalls.forEach(function (fapp, i) {
            apps.create(fapp, function (err, resp) {
                if (!err)
                    console.log(++i + " app(s) loaded")
            })
        })
        //    *************** END JSON CONFIGURATION
        // this is the cron job wich will execute after every 15 minutes
    var datamigration = function () {
        console.log('data is being updateing if any change found!');

        appcalls.forEach(function (appcall) {


            if (appcall.hasOwnProperty('credentials')) {
                var BIoptions = {
                    method: 'GET',
                    url: appcall.buildInfo.url,
                    rejectUnauthorized: false,
                    headers: {
                        authorization: "Basic " + btoa(appcall.credentials.username + ":" + appcall.credentials.password)
                    }
                }
                var TRoptions = {
                    method: 'GET',
                    url: appcall.testResults.url,
                    rejectUnauthorized: false,
                    headers: {
                        authorization: "Basic " + btoa(appcall.credentials.username + ":" + appcall.credentials.password)
                    }
                }
                var Voptions = {
                    method: 'GET',
                    url: appcall.versions
                }
            } else {

                var BIoptions = {
                    method: 'GET',
                    url: appcall.buildInfo.url,
                     rejectUnauthorized: false
                }
                var TRoptions = {
                    method: 'GET',
                    url: appcall.testResults.url,
                     rejectUnauthorized: false
                }
                var Voptions = {
                    method: 'GET',
                    url: appcall.versions
                }
            }
            //              start request

            request(BIoptions, function (b_error, BIresponse, BIbody) {
//                    console.log("BI Options" + JSON.stringify(BIoptions))
//                    console.log("BI Options" + JSON.stringify(BIresponse))
                    request(Voptions, function (error, v_response, Vbody) {
                    if(BIresponse === undefined){
                        console.log("failed updating data")
                    }
                       else if (!b_error && BIresponse.statusCode == 200) {
                                var jsonbody = JSON.parse(BIbody);
                                var versionBody = JSON.parse(Vbody);

                                request(TRoptions, function (tr_error, TRresponse, body) {
                                    if (!tr_error && TRresponse.statusCode == 200) {
                                        var jsontestResults = JSON.parse(body)
                                        appcall.model.model.find(function (err, resp) {
                                            console.log("resp.length: ", resp.length)
                                            if (resp.length == 0) {
                                                appcall.model.model.replaceOrCreate({
                                                    "appname": appcall.buildInfo.name,
                                                    "buildid": jsonbody.id,
                                                    "description": "",
                                                    "createdDatetimetamp": new Date().getTime(),
                                                    "timestamp": jsonbody.timestamp,
                                                    "url": jsonbody.url,
                                                    "buildInfo": jsonbody,
                                                    "testResults": jsontestResults,
                                                    "versions": versionBody
                                                }, function (err, resp) {
                                                    if (err)
                                                        console.log("Eror: ", err)
                                                    else
                                                        console.log("Data Saved!")
                                                })
                                            } else
                                                resp.forEach(function (obj, idx) {
                                                    if (obj.buildid === jsonbody.id)
                                                        var idExist = true
                                                            // searching existing build id
                                                    if (resp.length == ++idx) {
                                                        if (!idExist) {
                                                            appcall.model.model.replaceOrCreate({
                                                                "appname": appcall.buildInfo.name,
                                                                "buildid": jsonbody.id,
                                                                "description": "",
                                                                "createdDatetimetamp": new Date().getTime(),
                                                                "timestamp": jsonbody.timestamp,
                                                                "url": jsonbody.url,
                                                                "buildInfo": jsonbody,
                                                                "testResults": jsontestResults,
                                                                "versions": versionBody
                                                            }, function (err, resp) {
                                                                if (err)
                                                                    console.log("Err: ", err)
                                                                else
                                                                    console.log("Data Saved!")
                                                            })
                                                        } else {

                                                            console.log("data already exist")
                                                        }
                                                    }
                                                })
                                        })

                                    }

                                })
                            } else {
                                errors.forEach(function (httperr) {
                                    if (httperr.err_name == BIresponse.statusCode) {
                                        appcall.model.model.find(function (err, resp) {
                                            console.log("resp.length: ", resp.length)
                                            if (resp.length == 0) {
                                                appcall.model.model.replaceOrCreate({
                                                    "appname": appcall.buildInfo.name,
                                                    "createdDatetimetamp": new Date().getTime(),
                                                    "buildInfo": httperr,
                                                    "testResults": httperr
                                                }, function (err, resp) {
                                                    if (err)
                                                        console.log("Err: ", err)
                                                    else
                                                        console.log("First Data Saved!")
                                                })
                                            } else {
                                                resp.forEach(function (rec, idx) {
                                                    if (rec.buildid == "HTTP-ERR" + BIresponse.statusCode)
                                                        var errExist = true
                                                    if (resp.length == ++idx) {
                                                        if (!errExist) {
                                                            appcall.model.model.replaceOrCreate({
                                                                "appname": appcall.buildInfo.name,
                                                                "createdDatetimetamp": new Date().getTime(),
                                                                "buildid": "HTTP-ERR" + BIresponse.statusCode,
                                                                "buildInfo": httperr,
                                                                "testResults": httperr
                                                            }, function (err, resp) {
                                                                if (err)
                                                                    console.log("Err: ", err)
                                                                else
                                                                    console.log("Data Saved!")
                                                            })
                                                        }
                                                    }
                                                })
                                            }
                                        })


                                    }
                                })

                                if (BIresponse.statusCode == 404) {


                                }
                            }
                        }) //version request
                })
                //end request
        })

    }
    datamigration()
    new CronJob('*/15 * * * *', function () {
        datamigration()

    }, null, true, 'America/Los_Angeles')




}